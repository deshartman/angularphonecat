'use strict';

/* Controllers */

angular.module('phonecatControllers', [])

    .controller('PhoneListCtrl', function PhoneListCtrl($scope, Phone) {

        $scope.phones = Phone.query();
        $scope.orderProp = 'age';
    })

    .controller('PhoneDetailCtrl', function PhoneDetailCtrl($scope, $routeParams, Phone) {

        $scope.phone = Phone.get({phoneId:$routeParams.phoneId}, function (phone) {
            $scope.mainImageUrl = phone.images[0];
        });

        $scope.setImage = function (imageUrl) {
            $scope.mainImageUrl = imageUrl;
        }
    });